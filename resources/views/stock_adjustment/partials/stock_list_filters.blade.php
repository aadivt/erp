@if(empty($only) || in_array('stock_list_filter_location_id_from', $only))
<div class="col-md-3">
    <div class="form-group">
        {!! Form::label('stock_list_filter_location_id_from', 'Location:') !!}

        {!! Form::select('stock_list_filter_location_id_from', $business_locations, null, ['class' => 'form-control select2', 'style' => 'width:100%', 'placeholder' => __('lang_v1.all') ]); !!}
    </div>
</div>
@endif

@if(empty($only) || in_array('stock_list_filter_location_id_to', $only))
<!-- <div class="col-md-3">
    <div class="form-group">
        {!! Form::label('stock_list_filter_location_id_to', 'Location to:') !!}

        {!! Form::select('stock_list_filter_location_id_to', $business_locations, null, ['class' => 'form-control select2', 'style' => 'width:100%', 'placeholder' => __('lang_v1.all') ]); !!}
    </div>
</div> -->
@endif
@if(empty($only) || in_array('sell_list_filter_date_range', $only))
<div class="col-md-3">
    <div class="form-group">
        {!! Form::label('sell_list_filter_date_range', __('report.date_range') . ':') !!}
        {!! Form::text('sell_list_filter_date_range', null, ['placeholder' => __('lang_v1.select_a_date_range'), 'class' => 'form-control', 'readonly']); !!}
    </div>
</div>
@endif