<!-- business information here -->
<style>
	.second-tab tr,
	.second-tab td {
		border-bottom: 1px solid;
	}

	.second-tab {
		width: 100%;
	}

	.second-tab td {
		vertical-align: top;
		height: 40px;
	}

	.bill {
		height: 600px;
		/* overflow: scroll; */
	}

	hr.divider {
		margin: 0em !important;
		padding: 0em !important;
		border-width: 1px;
	}
</style>

<style media="print">
 @page {
  size: auto;
  margin: 0;
       }
</style>

<div class="w3-container" style="padding:10px; margin:10px;">
<div class="row" style="border-top:1px solid; border-left:1px solid; border-right:1px solid;   margin:15px !important; margin:0em !important; ">

	<div class="col-md-12" style="font-size:20px;text-align: center; font-weight: bold;  ">
		TAX INVOICE
	</div>
	<!-- Logo -->
	@if(!empty($receipt_details->logo))
	<!-- <img style="max-height: 120px; width: auto;" src="{{$receipt_details->logo}}" class="img img-responsive center-block"> -->
	@endif

	<!-- Header text -->
	@if(!empty($receipt_details->header_text))
	<div class="col-xs-12">
		<!-- {!! $receipt_details->header_text !!}  -->
	</div>
	@endif


	<div class="col-xs-12" style="padding:0px !important; margin:0px !important;">
		<table style="  width:100%">

			<tr style="  border-top: 1px solid; ">
				<td style="width:50%; border-right:1px solid;  padding-left:5px; font-weight:bold;" valign="top">
					<?php    // echo "<pre>"; print_r($receipt_details); echo "</pre>";    

					?>
					@if(!empty($receipt_details->location_name))
					<b>{{$receipt_details->location_name}}</b>
					@endif

					<p>

						<?php  //echo "<pre>"; print_r( $receipt_details) ; echo "</pre>";
						?>
						@if(!empty($receipt_details->address))
						<!-- <small class="text-center"> -->
						{!! $receipt_details->address !!}
						<!-- </small> -->
						@endif
						@if(!empty($receipt_details->contact))
						<br> {!! $receipt_details->contact !!}
						@endif
						@if(!empty($receipt_details->contact) && !empty($receipt_details->website))
						,
						@endif
						@if(!empty($receipt_details->website))
						<br>{{ $receipt_details->website }}
						@endif
						<!-- @if(!empty($receipt_details->location_custom_fields))
						<b>GSTffff:</b> {{ $receipt_details->location_custom_fields }}
						@endif --> 
						@if(!empty($receipt_details->branch_gst))
						<br/><b>GST:</b> {{ $receipt_details->branch_gst }}
						@endif  
					</p>

					<p>
						@if(!empty($receipt_details->sub_heading_line1))
						{{ $receipt_details->sub_heading_line1 }}
						@endif
						@if(!empty($receipt_details->sub_heading_line2))
						<br>{{ $receipt_details->sub_heading_line2 }}
						@endif
						@if(!empty($receipt_details->sub_heading_line3))
						<br>{{ $receipt_details->sub_heading_line3 }}
						@endif
						@if(!empty($receipt_details->sub_heading_line4))
						<br>{{ $receipt_details->sub_heading_line4 }}
						@endif
						@if(!empty($receipt_details->sub_heading_line5))
						<br>{{ $receipt_details->sub_heading_line5 }}
						@endif
						@if(!empty($receipt_details->tax_info1))
						<!-- <b>
					 	{{ $receipt_details->tax_label1 }}</b> {{ $receipt_details->tax_info1 }}
							@endif

							@if(!empty($receipt_details->tax_info2))
							<b>

					 		{{ $receipt_details->tax_label2 }}</b> {{ $receipt_details->tax_info2 }}
								@endif -->
					</p>
					<hr class="divider" />
					@if(!empty($receipt_details->customer_info))
					<b>{{ $receipt_details->customer_label }}</b> <br> {!! $receipt_details->customer_info !!} <br>
					@endif
					@if(!empty($receipt_details->client_id_label))
					<!-- <br /> -->
					<b>{{ $receipt_details->client_id_label }}</b> {{ $receipt_details->client_id }}
					@endif
					@if(!empty($receipt_details->customer_tax_label))
					<!-- <br /> -->
					<b>{{ $receipt_details->customer_tax_label }}</b> {{ $receipt_details->customer_tax_number }}
					@endif
					@if(!empty($receipt_details->customer_custom_fields))
					<!-- <br /> -->
					{!! $receipt_details->customer_custom_fields !!}
					@endif
					@if(!empty($receipt_details->sales_person_label))
					<br />
					<b>
						{{ $receipt_details->sales_person_label }}</b> {{ $receipt_details->sales_person }}
					@endif
					@if(!empty($receipt_details->commission_agent_label))
					<br />
					<strong>{{ $receipt_details->commission_agent_label }}</strong> {{ $receipt_details->commission_agent }}
					@endif
					@if(!empty($receipt_details->customer_rp_label))
					<br />
					<strong>{{ $receipt_details->customer_rp_label }}</strong> {{ $receipt_details->customer_total_rp }}
					@endif

				</td>
				<td valign="top" style="padding:0px; ">
					<table class="second-tab">
						<tr>
							<td style="padding-left:5px;">
								@if(!empty($receipt_details->invoice_heading))
								<!-- {!! $receipt_details->invoice_heading !!} -->
								@else
								<!-- Invoice No. -->
								@endif

								@if(!empty($receipt_details->invoice_no_prefix))
								<b>{!! $receipt_details->invoice_no_prefix !!}</b>
								@else
								Invoice No.
								@endif
								<br>
								<b>{{$receipt_details->invoice_no}}</b>
							</td>
							<td>
								@if(!empty( $receipt_details->date_label ))
								<b>{{$receipt_details->date_label}}</b>
								@else
								Dated
								@endif
								<br><b> {{$receipt_details->invoice_date}}</b>
							</td>
						</tr>
						<tr>
							<td style="padding-left:5px;">Delivery Note</td>
							<td>Mode/Terms of payment</td>
						</tr>
						<!-- 
						<tr>
							<td>Reference NO & Date</td>
							<td>Other References</td>
						</tr> -->
						<tr>
							<td style="padding-left:5px;">Buyers Order No</td>
							<td>Dated</td>
						</tr>
						<tr>
							<td style="padding-left:5px;">Delivery To <br />
								{{$receipt_details->delivered_to}}
							</td>
							<td>Delivery Note Date</td>
						</tr>
						<!-- <tr>
							<td>Dispatched through <br/>
						</td>  
							<td>Destination</td>
						</tr> -->
						<tr>
							<td style="padding-left:5px;">Bill of Lading/LR-RR No</td>
							<td>Motor Vehicle No. <br>
								{{$receipt_details->vehicle_number}}
							</td>
						</tr>
						<tr style="border-bottom:none !important">
							<td colspan="2" style="border-bottom:none !important">Terms of Delivery <br />
								{{$receipt_details->additional_notes}}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="row" style="border-top:1px solid;   border-left:1px solid ;  border-right:1px solid ;  border-bottom:1px solid ;  margin:15px !important; margin:0em !important; ">
	@includeIf('sale_pos.receipts.partial.common_repair_invoice')
	<!-- <div class="col-md-12" style="padding:0px !important; margin:0px !important; "> -->
	<div class="col-xs-12" style="padding:0px !important; margin:0px !important;">


		@php
		$p_width = 50;
		@endphp
		<!-- @if(!empty($receipt_details->item_discount_label))
			@php
			$p_width -= 15;
			@endphp
			@endif -->
		<table>
			<thead>
				<tr style=" border-bottom:1px solid !important;">
					<th width="{{$p_width}}%" style="  padding-left:5px; padding-right:5px;" style="margin:5px; padding:5px;">{{$receipt_details->table_product_label}}</th>
					<th class="text-center" style="  border-left:1px solid !important; " width="5%">HSN code</th>
					<th class="text-center" style="  border-left:1px solid !important;" width="5%">{{$receipt_details->table_qty_label}}</th>
					<th class="text-center" style="  border-left:1px solid !important;" width="5%">Per</th>
					<th class="text-center" style="  border-left:1px solid !important;" width="10%">{{$receipt_details->table_unit_price_label}}</th>
					<!-- @if(!empty($receipt_details->item_discount_label))
						<th class="text-center" width="15%">{{$receipt_details->item_discount_label}}</th>
						@endif -->
					<th class="text-center" style="  border-left:1px solid !important;" width="10%">Taxable value</th>
					<th class="text-center" style="  border-left:1px solid !important;" width="5%">
						TAX <br /> Rate
					</th>
					<th class="text-center" style="  border-left:1px solid !important;" width="10%">{{$receipt_details->table_subtotal_label}}</th>
				</tr>

			</thead>


			<tbody style="   ">
				@forelse($receipt_details->lines as $line)
				<tr style="border-top:1px solid;border-right:1px solid;border-bottom:1px solid;  ">
					<td style=" border-right:1px solid !important; padding-left:5px;">
						@if(!empty($line['image']))
						<img src="{{$line['image']}}" alt="Image" width="50" style="float: left; margin-right: 8px;">
						@endif
						{{$line['name']}}
						{{$line['product_variation']}} {{$line['variation']}}
						<!-- @if(!empty($line['sub_sku'])), {{$line['sub_sku']}} @endif @if(!empty($line['brand'])), {{$line['brand']}} @endif @if(!empty($line['cat_code'])), {{$line['cat_code']}}@endif
							@if(!empty($line['product_custom_fields'])), {{$line['product_custom_fields']}} @endif
							@if(!empty($line['sell_line_note'])) -->
						<br>
						<small>
							{{$line['sell_line_note']}}
						</small>
						@endif
						<!-- @if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}: {{$line['lot_number']}} @endif
							@if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}: {{$line['product_expiry']}} @endif

							@if(!empty($line['warranty_name'])) <br><small>{{$line['warranty_name']}} </small>@endif @if(!empty($line['warranty_exp_date'])) <small>- {{@format_date($line['warranty_exp_date'])}} </small>@endif
							@if(!empty($line['warranty_description'])) <small> {{$line['warranty_description'] ?? ''}}</small>@endif -->
					</td>
					<td style=" border-right:1px solid !important;" class="text-center">{{$line['cat_code']}}</td>
					<td style=" border-right:1px solid !important;" class="text-center">{{$line['quantity']}} </td>
					<td style=" border-right:1px solid !important;" class="text-center">{{$line['units']}} </td>
					<td style=" border-right:1px solid !important;" class="text-center">{{$line['unit_price_before_discount']}}</td>
					<!-- @if(!empty($receipt_details->item_discount_label))
						<td class="text-right">
							{{$line['line_discount'] ?? '0.00'}}
						</td>
						@endif -->

					<td style=" border-right:1px solid !important;" class="text-center">{{$line['price_exc_tax']}}</td>
					<td style=" border-right:1px solid !important;" class="text-center">{{$line['tax_percent']}}&percnt;</td>
					<td class="text-right" style="padding-right: 5px;">{{$line['line_total']}}</td>
				</tr>
				@if(!empty($line['modifiers']))
				@foreach($line['modifiers'] as $modifier)
				<tr>

					<td>
						{{$modifier['name']}} {{$modifier['variation']}}
						@if(!empty($modifier['sub_sku'])), {{$modifier['sub_sku']}} @endif @if(!empty($modifier['cat_code'])), {{$modifier['cat_code']}}@endif
						@if(!empty($modifier['sell_line_note']))({{$modifier['sell_line_note']}}) @endif
					</td>
					<td class="text-right">{{$modifier['quantity']}} {{$modifier['units']}} </td>
					<td class="text-right">{{$modifier['unit_price_inc_tax']}}</td>
					@if(!empty($receipt_details->item_discount_label))
					<td class="text-right">0.00</td>
					@endif
					<td class="text-right">{{$modifier['line_total']}}</td>
				</tr>
				@endforeach
				@endif
				@empty
				<tr>
					<td colspan="3">&nbsp;  </td>
				</tr>
				@endforelse

				<!-- <tr style="  border:none !important;  ">
					<td colspan="8">&nbsp; wqwq </td>
				</tr> -->
				<tr colspan="8" style="  padding-left:5px;" >
					<td style="margin:10px; padding-left:5px;" >
 
						<table style="border:1px solid white; width:50%; margin:10px; padding:10px;">
							<tr>
								@if( count($receipt_details->lines) <= 5) <tr style="  border-top: 2px solid white !important;  border-bottom: 2px solid white !important;  ">
									<td colspan="8">&nbsp; </td>
							</tr>
							<tr style=" border-top: 2px solid white !important;  border-bottom: 2px solid white !important;  ">
								<td colspan="8">&nbsp; </td>
							</tr>
							<tr style=" border-top: 2px solid white !important;  border-bottom: 2px solid white !important;  ">
								<td colspan="8"> &nbsp; </td>
							</tr>
							<tr style=" border-top: 2px solid white !important;  border-bottom: 2px solid white !important;  ">
								<td colspan="8">&nbsp; </td>
							</tr>
							<!-- <tr style=" border-top: 2px solid white !important;  border-bottom: 2px solid white !important;  ">
						<td colspan="8">&nbsp; </td>
					</tr>
					<tr style=" border-top: 2px solid white !important;  border-bottom: 2px solid white !important;  ">
						<td colspan="8"> &nbsp;</td>
					</tr> -->
							@else
							@for($i=8; $i <=count($receipt_details->lines) ; $i-- )
								<tr style=" border-top: 2px solid white !important;  border-bottom: 2px solid white !important;  ">
									<td colspan="8"> &nbsp;</td>
								</tr>
								@endfor
								@endif

				</tr>
		</table>
		</td>
		</tr>


		<!-- <tr style=" border-top: 2px solid white !important; ">
			<td colspan="8"> &nbsp;</td>
		</tr> -->

		<tr style="border-bottom: 1px solid black !important;">
			<td width="50%" colspan="2" style="border-top: 1px solid black !important;padding-left:5px;"> Total Quantity</td>
			<td width="5%" class="text-center" style="border :1px solid black;">{!! $receipt_details->total_quantity !!}</td>
			<td width="5%" colspan="4" style="border :1px solid black;padding-left:5px;"> Subtotal</td>
			<td width="10%" class="text-center" style="border :1px solid black;">{!! $receipt_details->subtotal !!}</td>
		</tr>

		<!-- <tr style="border: 1px solid black !important;">
							<td width="50%" colspan="8" style="border :1px solid black;padding-left:5px;"> Amount chargable (in words)
							<br/>
							<b>({{$receipt_details->total_in_words}})</b>
						</td>  
						</tr> -->

		</tbody>
		</table>
	</div>


		<!--   removed  row   --> 
 
	<div class="col-xs-6"> 
		<table class="">

			<!-- @if(!empty($receipt_details->payments))
			@foreach($receipt_details->payments as $payment)
			<tr>
				<td>{{$payment['method']}}</td>
				<td class="text-right">{{$payment['amount']}}</td>
				<td class="text-right">{{$payment['date']}}</td>
			</tr>
			@endforeach
			@endif -->

			<!-- Total Paid-->
			<!-- @if(!empty($receipt_details->total_paid))
			<tr>
				<th>
					{!! $receipt_details->total_paid_label !!}
				</th>
				<td class="text-right">
					{{$receipt_details->total_paid}}
				</td>
			</tr>
			@endif -->

			<!-- Total Due-->
			<!-- @if(!empty($receipt_details->total_due) && !empty($receipt_details->total_due_label))
			<tr>
				<th>
					{!! $receipt_details->total_due_label !!}
				</th>
				<td class="text-right">
					{{$receipt_details->total_due}}
				</td>
			</tr>
			@endif

			@if(!empty($receipt_details->all_due))
			<tr>
				<th>
					{!! $receipt_details->all_bal_label !!}
				</th>
				<td class="text-right">
					{{$receipt_details->all_due}}
				</td>
			</tr>
			@endif -->
		</table>
	</div>


	<div class="col-xs-6" style="padding:0px !important; margin:0px !important;">
		<div class="table-responsive">
			<table style=" width:100%;  border-left:1px solid !important; ">
				<tbody>
					<!-- @if(!empty($receipt_details->total_quantity_label))
					<tr class="color-555">
						<th style="width:70%">
							{!! $receipt_details->total_quantity_label !!}
						</th>
						<td class="text-right">
							{{$receipt_details->total_quantity}}
						</td>
					</tr>
					@endif -->
					<tr>
						<th style="width:70%; padding:4px;">
							{!! $receipt_details->subtotal_label !!}
						</th>
						<td class="text-right">
							{{$receipt_details->subtotal}}
						</td>
					</tr>
					@if(!empty($receipt_details->total_exempt_uf))
					<tr>
						<th style="width:70%; padding:4px;">
							@lang('lang_v1.exempt')
						</th>
						<td class="text-right">
							{{$receipt_details->total_exempt}}
						</td>
					</tr>
					@endif
					<!-- Shipping Charges -->
					@if(!empty($receipt_details->shipping_charges))
					<tr>
						<th style="width:70%; padding:4px;">
							{!! $receipt_details->shipping_charges_label !!}
						</th>
						<td class="text-right">
							{{$receipt_details->shipping_charges}}
						</td>
					</tr>
					@endif

					@if(!empty($receipt_details->packing_charge))
					<tr>
						<th style="width:70%;padding:4px; ">
							{!! $receipt_details->packing_charge_label !!}
						</th>
						<td class="text-right">
							{{$receipt_details->packing_charge}}
						</td>
					</tr>
					@endif

					<!-- Discount -->
					@if( !empty($receipt_details->discount) )
					<tr>
						<th>
							{!! $receipt_details->discount_label !!}
						</th>

						<td class="text-right">
							(-) {{$receipt_details->discount}}
						</td>
					</tr>
					@endif

					@if( !empty($receipt_details->total_line_discount) )
					<tr>
						<th>
							{!! $receipt_details->line_discount_label !!}
						</th>

						<td class="text-right">
							(-) {{$receipt_details->total_line_discount}}
						</td>
					</tr>
					@endif

					@if( !empty($receipt_details->additional_expenses) )
					@foreach($receipt_details->additional_expenses as $key => $val)
					<tr>
						<td style="padding-left:4px;">
							<b>{{$key}}:</b>
						</td>

						<td class="text-right">
							(+) {{$val}}
						</td>
					</tr>
					@endforeach
					@endif

					@if( !empty($receipt_details->reward_point_label) )
					<tr>
						<th>
							{!! $receipt_details->reward_point_label !!}
						</th>

						<td class="text-right">
							(-) {{$receipt_details->reward_point_amount}}
						</td>
					</tr>
					@endif

					<!-- Tax -->
					@if( !empty($receipt_details->tax) )
					<tr>
						<th>
							{!! $receipt_details->tax_label !!}
						</th>
						<td class="text-right">
							(+) {{$receipt_details->tax}}
						</td>
					</tr>
					@endif

					@if( $receipt_details->round_off_amount > 0)
					<tr>
						<th style="padding-left:4px;">
							{!! $receipt_details->round_off_label !!}
						</th>
						<td class="text-right" style="padding-left:4px;">
							{{$receipt_details->round_off}}
						</td>
					</tr>
					@endif

					<!-- Total -->
					<tr>
						<th style="padding:4px;">
							{!! $receipt_details->total_label !!}
						</th>
						<td class="text-right" style="padding-left:4px;">
							{{$receipt_details->total}}
							@if(!empty($receipt_details->total_in_words))
							<!-- <br>
								<small>({{$receipt_details->total_in_words}})</small> -->
							@endif
						</td>
					</tr>


					<tr>
						<td colspan="2" style="padding-left:4px;">
							@if(!empty($receipt_details->total_in_words))
							<b>({{$receipt_details->total_in_words}})</b>
							@endif
						</td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>



	<?php 
		$groups = array();
		$tax_names_array = array();	
	
	?>
	@foreach($receipt_details->lines as $line)
	<?php
	$k = $line['tax_name'];

	$tax_name_ar = explode('@',$line['tax_name']);
	$tax_name = $tax_name_ar[0];
	$tax_names_array[] = $tax_name;

	if (!array_key_exists(trim($k), $groups)) {
		if ($tax_name == "IGST") {

			$groups[$k] = array(
				'tax_percent' => $line['tax_percent'],
				'price_exc_tax' => $line['price_exc_tax'],
				'igst' => $line['group_tax_details'][0]['amount'],
				'total_tax' => $line['group_tax_details'][0]['calculated_tax']
			);
		} else {
			$groups[$k] = array(
				'tax_percent' => $line['tax_percent'],
				'price_exc_tax' => $line['price_exc_tax'],
				'cgst' => $line['group_tax_details'][0]['amount'],
				'cgst_tax' => $line['group_tax_details'][0]['calculated_tax'],

				'sgst' => $line['group_tax_details'][1]['amount'],
				'sgst_tax' => $line['group_tax_details'][1]['calculated_tax'],
				'total_tax' => $line['group_tax_details'][0]['calculated_tax'] + $line['group_tax_details'][1]['calculated_tax'],
			);
		}
	} else {

		if ($tax_name == "IGST") {

			$groups[$k]['tax_percent'] =   $line['tax_percent'];
			$groups[$k]['price_exc_tax'] += $line['price_exc_tax'];

			$groups[$k]['igst'] = $line['group_tax_details'][0]['amount'];
			$groups[$k]['total_tax'] += round($line['group_tax_details'][0]['calculated_tax'], 2);

		} else {

			$groups[$k]['tax_percent'] =   $line['tax_percent'];
			$groups[$k]['price_exc_tax'] += $line['price_exc_tax'];

			$groups[$k]['cgst'] = $line['group_tax_details'][0]['amount'];
			$groups[$k]['cgst_tax'] += round($line['group_tax_details'][0]['calculated_tax'], 2);

			$groups[$k]['sgst'] = $line['group_tax_details'][1]['amount'];
			$groups[$k]['sgst_tax'] +=  round($line['group_tax_details'][1]['calculated_tax'], 2);
			$groups[$k]['total_tax'] += round($line['group_tax_details'][0]['calculated_tax'], 2) + round($line['group_tax_details'][1]['calculated_tax'], 2);
		}
	}
	?>
	@endforeach


	<div class="col-xs-12" style="border-top:1px solid black;border-bottom: none;border-left:none !important; border-left: none !important; padding:0px !important; margin:0px !important; ">
		<table style="width: 100%;">
			<tr style="border-bottom:1px solid black !important; ">
				<th class="text-center" style="border-right:1px solid black;">Tax Rate </th>
				<th class="text-center" style="border-right:1px solid black;">Taxable Amount </th>
				<th class="text-center" style="border-right:1px solid black;">SGST % </th>
				<th class="text-center" style="border-right:1px solid black;">SGST Amount </th>
				<th class="text-center" style="border-right:1px solid black;">CGST % </th> 
				<th class="text-center" style="border-right:1px solid black;">CGST Amount</th>
				@if(in_array("IGST", $tax_names_array))
				<th class="text-center" style="border-right:1px solid black;">IGST % </th> 
				<!-- <th class="text-center" style="border-right:1px solid black;">IGST Amount</th> -->
				@endif
				<th class="text-center">Total Tax Amount</th>
			</tr>
			@foreach($groups as $tax)
			<tr style="border-bottom:1px solid black !important; ">
				<td class="text-center" style="border-right:1px solid black;">{{$tax['tax_percent'] }}</td>
				<td class="text-center" style="border-right:1px solid black;">{{$tax['price_exc_tax'] }} </td>
				<td class="text-center" style="border-right:1px solid black;">{{$tax['sgst'] }} </td>
				<td class="text-center" style="border-right:1px solid black;">{{$tax['sgst_tax'] }} </td>
				<td class="text-center" style="border-right:1px solid black;">{{$tax['cgst'] }} </td>
				<td class="text-center" style="border-right:1px solid black;">{{$tax['cgst_tax'] }} </td>
				@if(in_array("IGST", $tax_names_array))
				<td class="text-center" style="border-right:1px solid black;"> {{$tax['igst']}}</th> 
				@endif
				<td class="text-center">{{$tax['total_tax'] }} </td>

			</tr>
			@endforeach

		</table>

	</div>

	<?php
	// echo "<pre>";

	// print_r($tax_names_array);
	// print_r( $receipt_details->taxes);
	// print_r($receipt_details->lines);

	// echo "</pre>";


	/*
	<div class="border-bottom col-md-12">
		@if(empty($receipt_details->hide_price) && !empty($receipt_details->tax_summary_label) )
		<!-- tax -->
		@if(!empty($receipt_details->taxes))
		<table class="table table-slim table-bordered">
			<tr>
				<th colspan="2" class="text-center">{{$receipt_details->tax_summary_label}}</th>
			</tr>
			@foreach($receipt_details->taxes as $key => $val)
			<tr>
				<td class="text-center"><b>{{$key}}</b></td>
				<td class="text-center">{{$val}}</td>
			</tr>
			@endforeach
		</table>
		@endif
		@endif
	</div>

	*/
	?>
	@if(!empty($receipt_details->additional_notes))
	<!-- <div class="col-xs-12">
			<p>{!! nl2br($receipt_details->additional_notes) !!}</p>
		</div> -->
	@endif


	<!--   removed  row   -->

	@if(!empty($receipt_details->footer_text))
	<div class="@if($receipt_details->show_barcode || $receipt_details->show_qr_code) col-xs-8 @else col-xs-12 @endif" style="padding-left:5px !important;"> 
		{!! $receipt_details->footer_text !!}
	</div>
	@endif
 
	<div class="col-xs-12" style="border-top:1px solid black;border-bottom: none;border-left:none !important; border-left: none !important;  padding-left:5px;">
	<b>Declaration:</b> We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.
	</div>

	<div class="col-xs-12" style="padding:0px !important; margin:0px !important;">
		<div class="table-responsive">
			<table style="  width:100%;  ">
				<tbody>
					<tr style="border-top: 1px solid !important;">
						<td style="border-right: 1px solid !important;padding-left:4px;">Receviers Signature <br /> <br /><br /><br /> </td>
						<td style=" padding-left:4px;"> Authorized Signature <br /><br /><br /><br />  </td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		@if($receipt_details->show_barcode || $receipt_details->show_qr_code)
		<div class="@if(!empty($receipt_details->footer_text)) col-xs-4 @else col-xs-12 @endif text-center">
			@if($receipt_details->show_barcode)
			{{-- Barcode --}}
			<img class="center-block" src="data:image/png;base64,{{DNS1D::getBarcodePNG($receipt_details->invoice_no, 'C128', 2,30,array(39, 48, 54), true)}}">
			@endif

			@if($receipt_details->show_qr_code && !empty($receipt_details->qr_code_text))
			<img class="center-block mt-5" src="data:image/png;base64,{{DNS2D::getBarcodePNG($receipt_details->qr_code_text, 'QRCODE', 3, 3, [39, 48, 54])}}">
			@endif
		</div>
		@endif
	</div>
</div>
</div>