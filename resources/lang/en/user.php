<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Business Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during registration of a business.
    |
    */
    'edit_profile' => 'Edit Profile',
    'change_password' => 'Change Password',
    'current_password' => 'Current password',
    'new_password' => 'New password',
    'confirm_new_password' => 'Confirm new password',
    'user_management' => 'Employee Management',
    'roles' => 'Roles',
    'users' => 'Employees',
    'manage_roles' => 'Manage roles',
    'all_roles' => 'All roles',
    'add_role' => 'Add role',
    'role_name' => 'Role Name',
    'permissions' => 'Permissions',
    'role_added' => 'Role added successfully',
    'role_already_exists' => 'Role with the same name already exists',
    'edit_role' => 'Edit role',
    'role_updated' => 'Role updated successfully',
    'role_deleted' => 'Role deleted successfully',
    'manage_users' => 'Manage Employees',
    'all_users' => 'All Employees',
    'name' =>'Name',
    'role' => 'Role',
    'add_user' => 'Add Employee',
    'user_added' => 'Employee added successfully',
    'role_is_default' => 'You can not modify this role',
    'edit_user' => 'Edit employee',
    'leave_password_blank' => "Leave password field blank if you don't want to update password",
    'user_update_success' => 'Employee updated successfully',
    'user_delete_success' => 'Employee deleted successfully',
];
