$(document).ready(function () {
        $(document).on('change', '.subcat', function (e) {
                if ($('.subcat').is(":checked")) { 
                        $('#subcatDiv').show();
                        $('#subcategory').html('');
                        $.ajax({
                                method: 'GET',
                                url: '/getCategories',
                                success: function (data) {
                                        var selOpts = "";
                                        for (i = 0; i < data.length; i++) {
                                                var id = data[i]['id'];
                                                var val = data[i]['name'];
                                                selOpts += "<option value='" + id + "'>" + val + "</option>";
                                        }
                                        $('#category').html(selOpts);
                                },
                        });
                }
                else {
                        $('#subcategory').html(''); 
                        $('#subcatDiv').hide();
                }

        });

        $("#expense_category_id").change(function () {
                var cat_id =  $('#expense_category_id').val();
                $('#subcategoryOnchange').html('');
                $.ajax({
                        method: 'POST',
                        url: '/getSubCategories',
                        data: { category_id: cat_id },
                        success: function (data) { 
                                var selOpts = "";
                                if( data.length > 0){ 
                                        for (i = 0; i < data.length; i++) {
                                                var id = data[i]['id'];
                                                var val = data[i]['name'];
                                                selOpts += "<option value='" + val + "'>" + val + "</option>";
                                        }
                                }
                                else{ 
                                        var selOpts = "";
                                        selOpts += "<option value=''>No sub categories</option>";  
                                }
                               
                                $('#subcategoryOnchange').append(selOpts);
                        },
                });

        });

        $(".expense_category").change(function () {
                var cat_id =  $('.expense_category').val();
                $('#expense_subcategory').html('');
                $.ajax({
                        method: 'POST',
                        url: '/getSubCategories',
                        data: { category_id: cat_id },
                        success: function (data) { 
                                var selOpts = "";
                                if( data.length > 0){ 
                                        selOpts = "<option value=''> All </option>";
                                        for (i = 0; i < data.length; i++) {
                                                var id = data[i]['id'];
                                                var val = data[i]['name'];
                                                selOpts += "<option value='" + val + "'>" + val + "</option>";
                                        }
                                }
                                else{ 
                                        var selOpts = "";
                                        selOpts += "<option value=''>No sub categories</option>";  
                                }
                               
                                $('#expense_subcategory').append(selOpts);
                        },
                });

        });
        // 

});